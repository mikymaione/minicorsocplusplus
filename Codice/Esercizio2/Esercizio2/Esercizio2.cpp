#include "funIO.h"

using namespace std;

int main()
{
	funIO fIO;

	// leggi il file
	auto complessi = fIO.LeggiFile();

	// fai i calcoli su tutti i numeri
	fIO.ScriviNelFile("coniugato dei complessi con modulo superiore a tre");
	for (size_t i = 0; i < complessi.size(); i++)
	{
		if (complessi[i].modulo() > 3)
		{
			fIO.ScriviNelFile(complessi[i].coniugato().stampa());
		}
	}


	complesso s(0, 0);
	for (size_t i = 0; i < complessi.size(); i++)
	{
		if (complessi[i].re < 0)
		{
			s = s.somma(complessi[i]);
		}
	}

	fIO.ScriviNelFile("somma dei complessi con parte reale negativa");
	fIO.ScriviNelFile(s.stampa());


	complesso p(1, 1);
	for (size_t i = 0; i < 3; i++)
	{
		p = p.prodotto(complessi[i]);
	}

	fIO.ScriviNelFile("prodotto dei primi tre complessi");
	fIO.ScriviNelFile(p.stampa());
}