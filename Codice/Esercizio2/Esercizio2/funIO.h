#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "complesso.h"

using namespace std;

class funIO
{
private:
	ifstream input;
	ofstream output;

public:
	funIO()
	{
		input.open("complessi.in");
		output.open("complessi.out");
	}

	vector<complesso> LeggiFile()
	{
		vector<complesso> ciro;

		while (input.eof() == false)
		{
			string re, im;

			input >> re;
			input >> im;
			
			complesso c(stof(re), stof(im));

			ciro.push_back(c);
		}
		
		return ciro;
	};

	void ScriviNelFile(string s)
	{
		cout << s << endl;
		output << s << endl;
	};

};