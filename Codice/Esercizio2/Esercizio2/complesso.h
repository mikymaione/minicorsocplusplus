#pragma once

#include <iostream>
#include <cmath>

using namespace std;

struct complesso
{
	float re, im;

	complesso(float re_, float im_)
	{
		re = re_;
		im = im_;
	}

	complesso coniugato()
	{
		complesso c(re, -1 * im);

		return c;
	};

	complesso somma(complesso b)
	{
		complesso c(im + b.im, re + b.re);

		return c;
	};

	complesso prodotto(complesso b)
	{
		complesso c(re * b.re, im * b.im);

		return c;
	};

	double modulo()
	{
		return sqrt(re * re + im * im);
	};

	string stampa()
	{
		return to_string(re) + " " + to_string(im) + "i\n";
	};

};