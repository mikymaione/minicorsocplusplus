#include <iostream>
#include <ctime>

using namespace std;

/*
creare una matrice statica 10x10 di numeri casuali interi e trovare il numero pi� piccolo e il numero pi� grande
*/

int main()
{
	srand(time(NULL));

	// array 10x10
	int matrice[10][10];

	// ciclo for X 1 -> 10
	// -ciclo for Y 1 -> 10
	// --rand() % 100; [0-100]

	for (size_t x = 0; x < 10; x++)
	{
		for (size_t y = 0; y < 10; y++)
		{
			matrice[x][y] = rand() % 90;
		}
	}

	// ciclo for X 1 -> 10
	// -ciclo for Y 1 -> 10
	// --if e < min
	int min = 99999;
	int max = 0;

	for (size_t x = 0; x < 10; x++)
	{
		for (size_t y = 0; y < 10; y++)
		{
			if (matrice[x][y] < min)
			{
				min = matrice[x][y];
			}

			if (matrice[x][y] > max)
			{
				max = matrice[x][y];
			}
		}
	}

	//print min
	//print max
	cout << "il minimo: " << min << endl;
	cout << "il massimo: " << max << endl;

	system("pause");

	return 0;
}