#pragma once

struct robot
{
	float x, y, vx, vy;

	robot() {}
	robot(float x, float y, float vx, float vy) :
		x(x), y(y), vx(vx), vy(vy) {}	
};

// definizioni
bool nelCerchio(robot a);
robot nuovaSit(robot a, float d);
