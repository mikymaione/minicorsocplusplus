#include <cmath>

#include "strutture.h"

using namespace std;

// implementazione
bool nelCerchio(robot a)
{
	return sqrt(a.x*a.x + a.y*a.y) <= 1;
};

robot nuovaSit(robot a, float d)
{
	return robot(a.x + a.vx * d, a.y + a.vy * d, a.vx, a.vy);
};
