#include <iostream>
#include <fstream>

#include "strutture.h"

using namespace std;

int NumeroRigheFile(string nomeFile)
{
	int n = 0;
	string str; 
	
	ifstream input(nomeFile);
	
	while (getline(input, str))
		n++;
		
	return n;
}

void LeggiTuttoIlFile(string nomeFile, robot vettore[], int N)
{
	int i = 0;
	ifstream input(nomeFile);	
		
	while (input.eof() == false)
	{
		float x, y, vx, vy;
		input >> x >> y >> vx >> vy;

		robot r(x, y, vx, vy);
		
		vettore[i];
		i++;
		
		if (i == N)
			break;
	}
}

int main()
{
	int N = NumeroRigheFile("robot.in");

	robot robots[N];

	LeggiTuttoIlFile("robot.in", robots, N);
	
	cout << "ci sono " << N << " robot così distribuiti:" << endl;

	for (int i = 0; i < N; i++)
	{
		cout << "robot alla posizione (" << robots[i].x << "," << robots[i].y << ") con componenti velocità (" << robots[i].vx << "," << robots[i].vy << ")" << endl;
	}
	
	int num_robot_raggio_1 = 0;
	for (int i = 0; i < N; i++)
	{
		if (nelCerchio(robots[i]))
		{
			num_robot_raggio_1++;
		}		
	}
	cout << "in numero di robot dentro al cerchio di raggio 1 è: " << num_robot_raggio_1 << endl;

	int A = 0;
	robot appoggio[N];
	for (int i = 0; i < N; i++)
	{
		appoggio[A] = nuovaSit(robots[i], 1);
		A++;
	}

	int num_robot_raggio_1_var = 0;
	for (int i = 0; i < A; i++)
	{
		if (nelCerchio(appoggio[i]))
		{
			num_robot_raggio_1_var++;
		}		
	}
	cout << "il numero di robot dentro al cerchio di raggio 1 dopo un secondo è: " << num_robot_raggio_1_var << endl;
	
	int num_robot_fuori_var1 = A - num_robot_raggio_1_var;
	int num_robot_fuori_var2 = N - num_robot_raggio_1;

	return 0;
}
