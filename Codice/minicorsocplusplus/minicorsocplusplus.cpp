#include <iostream> // varie funzioni di Input/Output
#include <string> // varie funzioni della classe String
#include <vector> // varie funzioni della classe Vector

using namespace std; // spazio delle funzioni STDandard

void TipiDiDati()
{
	bool Paperinick = false;

	int Topolino = -5; //32bit = 2^32 numeri da [-(2^32)/2; +(2^32)/2]
	unsigned int Topolina = 5; //32bit = 2^32 numeri da [0; +(2^32)]

	long Paperino = 1000; //64bit = 2^64 numeri da [-(2^64)/2; +(2^64)/2]
	unsigned long Paperina = 5000; //64bit = 2^64 numeri da [0; +(2^64)]

	float CuorDiPietra = 17.12; //32bit dipende quanti decimali usi, senza decimali � come unsigned int
	double Paperone = 25.56; //64bit dipende quanti decimali usi, senza decimali � come unsigned long

	char Pluto = 'g'; //8bit, 1 solo carattere	
	char Archimede[] = "Archimede Pitagorico"; //20 � 8bit, 20 caratteri, allocazione statica della scritta Archimede Pitagorico

	string Pico = "Pico De Paperis"; // classe String per memorizzare le stringhe (le scritte)

	const double Pi = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679; // costante, non pu� mai cambiare
}

bool SonoPronto()
{
	return true;
}

void StruttureDiControllo(unsigned int Topolina, int Paperino)
{
	if (Topolina == 3)
	{

	}
	else if (Topolina == 6)
	{

	}
	else if (Topolina == 12)
	{

	}
	else if (Topolina == 74)
	{

	}
	else
	{

	}

	for (size_t Topolino = 0; Topolino < 12457; Topolino++) // size_t = unsigned int, usato per le dimensioni degli insiemi, � molto elegante usarlo per i cicli for
	{
		if (Topolina == 777)
			break;
	}

	for (size_t SoloIPari = 0; SoloIPari < 20; SoloIPari = SoloIPari + 1)
	{

	}

	size_t Pippo = 0;
	while (Pippo < 12457)
	{
		if (Topolina == 777)
			break;

		Pippo++;
	}

	while (SonoPronto())
	{
		if (Topolina == 666)
			break;
	}
}

void ArrayStatici()
{
	int NonnaPapera[71]; // 71 numeri interi
	int Clarabella[] = { 4, 8, 2, 3, 5 }; // 5 numeri interi

	int Matrice2D[15][10]; // 15�10 puoi rappresentare una scacchiera
	int Matrice3D[15][10][7]; // 15�10�7 puoi rappresentare dei mattoncini lego
	int Matrice4D[15][10][7][100]; // 15�10�7�100 puoi rappresentare uno spazio in 3D e 100 instanti di tempo

	// in questo punto tutte le variabili muoiono = vengono deallocate
}

void ArrayDinamici()
{
	int* NonnaPapera = new int[71]; // 71 numeri interi
	int* Clarabella = new int[5]{ 4, 8, 2, 3, 5 }; // 5 numeri interi

	// 15�10 puoi rappresentare una scacchiera
	int** Matrice2D = new int*[15];

	for (size_t i = 0; i < 15; i++)
		Matrice2D[i] = new int[10];
	// 15�10 puoi rappresentare una scacchiera


	// inizio ad uccidere tutte le variabili = le dealloco
	delete[] NonnaPapera;
	delete[] Clarabella;


	for (size_t i = 0; i < 15; i++)
		delete[] Matrice2D[i];

	delete[] Matrice2D;
}

void ArrayModerni()
{
	vector<int> NonnaPapera;
	vector<int> Clarabella = { 4, 8, 2, 3, 5 };

	vector<vector<int>> Matrice2D; // puoi rappresentare una scacchiera
	vector<vector<vector<int>>> Matrice3D; // puoi rappresentare dei mattoncini lego
	vector<vector<vector<vector<int>>>> Matrice4D; // puoi rappresentare uno spazio in 3D e 100 instanti di tempo

	// in questo punto tutte le variabili muoiono = vengono deallocate
}

unsigned long Fattoriale(unsigned long n)
{
	unsigned long r = 1;

	for (unsigned long k = 1; k <= n; k++)
	{
		r = r * k;
	}

	return r;
}

void InvertiParole()
{
	unsigned short n;

	cout << "Metti il n di parole: ";
	cin >> n;

	auto parole = new string[n];

	for (size_t x = 1; x <= n; x++)
	{
		cout << "Metti la parola #" << x << ": ";
		cin >> parole[x - 1];
	}

	delete[] parole;
}

int main()
{
	cout << "Benvenuto nel mini corso di C++" << endl;

	unsigned long n;

	cout << "Metti il n per il fattoriale: ";
	cin >> n;

	cout << "Fattoriale di " << n << ": " << Fattoriale(n) << endl;

	system("pause");

	return 0;
}